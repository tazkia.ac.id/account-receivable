package id.ac.tazkia.finance.receivables;

import id.ac.tazkia.finance.receivables.bank.Bank;
import id.ac.tazkia.finance.receivables.bank.BankRepository;
import id.ac.tazkia.finance.receivables.common.StatusRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BankRepositoryTests {

    @Autowired private BankRepository bankRepository;

    @BeforeEach
    public void clearDatabase() {
        bankRepository.deleteAll().block();
    }

    @Test
    public void testCrud() {
        Bank bank = new Bank();
        bank.setCode("B-001");
        bank.setName("Bank 001");

        String bankId = bankRepository.save(bank).doOnSuccess(b -> {
            System.out.println("Bank sudah disimpan");
        }).block().getId();

        Assertions.assertNotNull(bankId);

        Bank bx = bankRepository.findById(bankId)
                .doOnSuccess(b -> {
                    System.out.println("Bank dengan id "+bankId+" ditemukan di database");
                }).block();

        Assertions.assertNotNull(bx);
        Assertions.assertEquals("B-001", bx.getCode());
        Assertions.assertEquals("Bank 001", bx.getName());
        Assertions.assertEquals(StatusRecord.ACTIVE, bx.getStatusRecord());

        bx.setCode("B-001x");
        bx.setName("Bank 001x");
        bx.setStatusRecord(StatusRecord.INACTIVE);
        bankRepository.save(bx)
                .block();

        bankRepository.findByCode("B-001x")
                .doOnSuccess(b -> System.out.println("Bank dengan code B-001x ada di database"))
                .subscribe(by -> {
                    Assertions.assertNotNull(by);
                    Assertions.assertEquals("B-001x", by.getCode());
                    Assertions.assertEquals("Bank 001x", by.getName());
                    Assertions.assertEquals(StatusRecord.INACTIVE, by.getStatusRecord());
                });
    }
}
