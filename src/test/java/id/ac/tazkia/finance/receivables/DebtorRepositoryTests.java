package id.ac.tazkia.finance.receivables;

import com.github.javafaker.Faker;
import id.ac.tazkia.finance.receivables.debtor.Debtor;
import id.ac.tazkia.finance.receivables.debtor.DebtorRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootTest
public class DebtorRepositoryTests {
    @Autowired private DebtorRepository debtorRepository;

    @BeforeEach
    public void resetDatabase() {
        debtorRepository.deleteAll().block();

        Faker faker = new Faker();

        Flux.range(1,100)
                .subscribe(i -> {
                    String code = ("D-"+String.format("%03d", i));
                    Debtor d = new Debtor();
                    d.setCode(code);
                    d.setName(faker.name().fullName());
                    d.setEmail(faker.internet().emailAddress());
                    d.setMobilePhone(faker.phoneNumber().cellPhone());
                    System.out.println("Saving debtor "+d);
                    debtorRepository.save(d).block();
                });
    }

    @Test
    public void testFindAllPagination() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        AtomicInteger counter = new AtomicInteger(0);

        Integer page = 3;
        Integer size = 20;

        Flux<Debtor> result = debtorRepository.findAllDebtor(page, size);
        result
            .doOnComplete(() -> {
                Assertions.assertEquals(size, counter.get());
                countDownLatch.countDown();
            })
            .subscribe(debtor -> {
                int debtorCode = counter.incrementAndGet() + (page * size);
                Assertions.assertNotNull(debtor.getCode());
                Assertions.assertEquals("D-"+String.format("%03d", debtorCode), debtor.getCode());
                System.out.println("Debtor : "+debtor);
            });

        countDownLatch.await();
    }
}
