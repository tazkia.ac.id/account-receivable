package id.ac.tazkia.finance.receivables;

import com.github.javafaker.Faker;
import id.ac.tazkia.finance.receivables.debtor.Debtor;
import id.ac.tazkia.finance.receivables.debtor.DebtorRepository;
import id.ac.tazkia.finance.receivables.invoice.Invoice;
import id.ac.tazkia.finance.receivables.invoice.InvoiceRepository;
import id.ac.tazkia.finance.receivables.type.InvoiceType;
import id.ac.tazkia.finance.receivables.type.InvoiceTypeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Locale;

@SpringBootTest
public class InvoiceRepositoryTests {
    @Autowired private DebtorRepository debtorRepository;
    @Autowired private InvoiceTypeRepository invoiceTypeRepository;
    @Autowired private InvoiceRepository invoiceRepository;

    private Faker faker = new Faker(new Locale("id", "id"));

    @BeforeEach
    public void resetDatabase() {
        invoiceRepository.deleteAll()
                .then(invoiceTypeRepository.deleteAll())
                .then(debtorRepository.deleteAll())
        .block();
    }

    @Test
    public void testInsertUsingRepository() {
        Debtor debtor = new Debtor();
        debtor.setCode(faker.number().digits(10));
        debtor.setName(faker.name().fullName());
        debtor.setEmail(faker.internet().emailAddress());
        debtor.setMobilePhone(faker.phoneNumber().cellPhone());

        InvoiceType registrationFee = new InvoiceType();
        registrationFee.setCode("R-001");
        registrationFee.setName("Registration Fee");

        Mono.zip(debtorRepository.save(debtor),
                invoiceTypeRepository.save(registrationFee))
                .flatMap(tupleDebtorInvoiceType -> {
                    Invoice invoice = new Invoice();
                    invoice.setIdDebtor(tupleDebtorInvoiceType.getT1().getId());
                    invoice.setIdInvoiceType(tupleDebtorInvoiceType.getT2().getId());
                    invoice.setCode(faker.number().digits(15));
                    invoice.setDescription("Test Invoice");
                    invoice.setInvoiceAmount(new BigDecimal("100000"));
                    return invoiceRepository.save(invoice);
                }).subscribe(ix -> {
                System.out.println("Invoice after save");
                Assertions.assertNotNull(ix.getId());
        });
    }
}
