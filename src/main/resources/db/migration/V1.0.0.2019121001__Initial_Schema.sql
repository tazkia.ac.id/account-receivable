create table invoice_type (
  id varchar (36),
  code varchar (50) not null,
  name varchar (100) not null,
  status_record varchar (50) not null,
  primary key (id),
  unique (code)
);

create table debtor (
  id varchar (36),
  code varchar (50) not null,
  name varchar (255) not null,
  email varchar (255),
  mobile_phone varchar (255),
  status_record varchar (50) not null,
  primary key (id),
  unique (code)
);

create table invoice (
  id varchar (36),
  code varchar (50) not null,
  id_invoice_type varchar (36) not null,
  id_debtor varchar (36) not null,
  description varchar (255) not null,
  invoice_amount decimal (19,2) not null,
  payment_amount decimal (19,2) not null,
  create_time timestamp not null,
  issue_date date not null,
  due_date date not null,
  status_payment varchar (100) not null,
  status_record varchar (50) not null,
  primary key (id),
  unique (code),
  foreign key (id_invoice_type) references invoice_type(id),
  foreign key (id_debtor) references debtor(id)
);

create table schedule (
  id varchar (36),
  status_record varchar (50) not null,
  primary key (id)
);

create table invoice_schedules (
  id_invoice varchar (36),
  id_schedule varchar (36),
  status_record varchar (50) not null,
  primary key (id_invoice, id_schedule),
  foreign key (id_invoice) references invoice(id),
  foreign key (id_schedule) references schedule(id)
);

create table payment_schedule (
  id varchar (36),
  id_schedule varchar (36) not null,
  payment_order integer not null,
  payment_date date not null,
  amount decimal (19,2) not null,
  status_record varchar (50) not null,
  primary key (id),
  foreign key (id_schedule) references schedule(id)
);

create table bank (
  id varchar (36),
  code varchar (50) not null,
  name varchar (100) not null,
  status_record varchar (50) not null,
  primary key (id),
  unique (code)
);

create table bank_account (
  id varchar (36),
  id_bank varchar (36) not null,
  account_number varchar (50) not null,
  account_name varchar (100) not null,
  status_record varchar (50) not null,
  primary key (id),
  unique (account_number),
  foreign key (id_bank) references bank (id)
);

create table virtual_account (
  id varchar (36),
  id_bank_account varchar (36) not null,
  id_payment_schedule varchar (36) not null,
  virtual_account_number varchar (50) not null,
  virtual_account_name varchar (100) not null,
  amount decimal (19,2) not null,
  virtual_account_type varchar (50) not null,
  status_record varchar (50) not null,
  primary key (id),
  foreign key (id_bank_account) references bank (id),
  foreign key (id_payment_schedule) references payment_schedule (id)
);

create table virtual_account_payment (
  id varchar (36),
  id_virtual_account varchar (36) not null,
  payment_time timestamp not null,
  payment_reference varchar (100) not null,
  payment_amount decimal (19,2) not null,
  primary key (id),
  foreign key (id_virtual_account) references virtual_account (id)
);

create table payment (
  id varchar (36),
  id_payment_schedule varchar (36) not null,
  payment_time timestamp not null,
  payment_amount decimal (19,2) not null,
  payment_type varchar (100) not null,
  id_virtual_account_payment varchar (36),
  primary key (id),
  foreign key (id_payment_schedule) references payment_schedule (id),
  foreign key (id_virtual_account_payment) references virtual_account_payment (id)
);
