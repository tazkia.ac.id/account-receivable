package id.ac.tazkia.finance.receivables.debtor;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface DebtorRepository extends ReactiveCrudRepository<Debtor, String> {
    @Query("select * from debtor order by code limit :size offset (:page * :size)")
    Flux<Debtor> findAllDebtor(Integer page, Integer size);
}
