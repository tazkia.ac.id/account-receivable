package id.ac.tazkia.finance.receivables.debtor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Controller
public class DebtorController {
    @Autowired private DebtorRepository debtorRepository;

    @GetMapping("/debtor/list")
    public ModelMap findAll(
            @RequestParam(name = "page", defaultValue = "0") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size) {

        return new ModelMap()
                .addAttribute("page", page)
                .addAttribute("size", size)
                .addAttribute("count", debtorRepository.count())
                .addAttribute("debtorList",
                        debtorRepository.findAllDebtor(page, size));
    }

    @GetMapping("/debtor/form")
    public ModelMap displayForm(@RequestParam(required = false) String id) {
        return new ModelMap()
                .addAttribute("debtor",
                        StringUtils.hasText(id) ?
                                debtorRepository.findById(id) :
                                Mono.just(new Debtor()));
    }

    @PostMapping("/debtor/form")
    public Mono<String> processForm(@ModelAttribute @Valid Debtor debtor, BindingResult errors, SessionStatus status) {
        return errors.hasErrors() ?
                Mono.just("debtor/form") :
                debtorRepository.save(debtor)
                        .map(b ->{
                            status.setComplete();
                            return "redirect:list";
                        });
    }
}
