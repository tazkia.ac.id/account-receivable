package id.ac.tazkia.finance.receivables;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountReceivableApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountReceivableApplication.class, args);
	}

}
