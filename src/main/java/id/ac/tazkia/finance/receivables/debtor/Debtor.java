package id.ac.tazkia.finance.receivables.debtor;

import id.ac.tazkia.finance.receivables.common.BasePersistable;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class Debtor extends BasePersistable {

    @NotEmpty @Size(min = 3, max = 50)
    private String code;

    @NotEmpty @Size(min = 3, max = 255)
    private String name;

    @Email
    private String email;
    private String mobilePhone;
}
