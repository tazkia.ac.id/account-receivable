package id.ac.tazkia.finance.receivables.bank;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface BankRepository extends ReactiveCrudRepository<Bank, String> {

    @Query("select * from bank where code = :code")
    Mono<Bank> findByCode(String code);
}
