package id.ac.tazkia.finance.receivables.invoice;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface InvoiceRepository extends ReactiveCrudRepository<Invoice, String> {
}
