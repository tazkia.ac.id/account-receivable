package id.ac.tazkia.finance.receivables.common;

public enum StatusPayment {
    UNPAID,PARTIALLY_PAID,FULLY_PAID
}