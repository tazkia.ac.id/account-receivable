package id.ac.tazkia.finance.receivables.bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Controller
public class BankController {

    @Autowired
    private BankRepository bankRepository;

    @GetMapping("/bank/list")
    public ModelMap allBank() {
        return new ModelMap()
                .addAttribute("bankList",
                        bankRepository.findAll());
    }

    @GetMapping("/bank/form")
    public ModelMap displayForm(@RequestParam(required = false) String id) {
        return new ModelMap()
                .addAttribute("bank",
                        StringUtils.hasText(id) ?
                                bankRepository.findById(id) :
                                Mono.just(new Bank()));
    }

    @PostMapping("/bank/form")
    public Mono<String> processForm(@ModelAttribute @Valid Bank bank, BindingResult errors, SessionStatus status) {
        return errors.hasErrors() ?
            Mono.just("bank/form") :
            bankRepository.save(bank)
                .map(b ->{
                    status.setComplete();
                    return "redirect:list";
                });
    }

    @GetMapping("/bank/delete")
    public Mono<String> delete(@RequestParam String id) {
        return bankRepository
                .deleteById(id)
                .thenReturn("redirect:list");
    }
}
