package id.ac.tazkia.finance.receivables.common;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.UUID;

@Data
public class BasePersistable implements Persistable<String> {

    @Id
    private String id;
    private StatusRecord statusRecord = StatusRecord.ACTIVE;

    @Override
    public boolean isNew() {
        if(!StringUtils.hasText(id)) id = null;
        boolean idIsNull = Objects.isNull(id);
        this.id = idIsNull ? UUID.randomUUID().toString() : this.id;
        return idIsNull;
    }
}
