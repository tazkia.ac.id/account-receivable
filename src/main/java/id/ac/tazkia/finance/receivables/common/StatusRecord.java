package id.ac.tazkia.finance.receivables.common;

public enum StatusRecord {
    ACTIVE, INACTIVE
}
