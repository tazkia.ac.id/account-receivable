package id.ac.tazkia.finance.receivables.type;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface InvoiceTypeRepository extends ReactiveCrudRepository<InvoiceType, String> {
    @Query("select * from bank where code = :code")
    Mono<InvoiceType> findByCode(String code);
}
