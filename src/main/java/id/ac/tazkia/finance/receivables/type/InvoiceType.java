package id.ac.tazkia.finance.receivables.type;

import id.ac.tazkia.finance.receivables.common.BasePersistable;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class InvoiceType extends BasePersistable {
    @NotEmpty
    @Size(min = 3, max = 50)
    private String code;

    @Size(min = 3, max = 255)
    private String name;
}
