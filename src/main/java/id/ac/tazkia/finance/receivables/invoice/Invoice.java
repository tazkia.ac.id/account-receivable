package id.ac.tazkia.finance.receivables.invoice;

import id.ac.tazkia.finance.receivables.common.BasePersistable;
import id.ac.tazkia.finance.receivables.common.StatusPayment;
import id.ac.tazkia.finance.receivables.common.StatusRecord;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class Invoice extends BasePersistable {

    @NotEmpty
    private String idInvoiceType;

    @NotEmpty
    private String idDebtor;

    @NotEmpty @Size(min = 3, max = 50)
    private String code;

    @NotEmpty @Size(min = 3, max = 255)
    private String description;

    @NotNull @Min(1)
    private BigDecimal invoiceAmount;

    private BigDecimal paymentAmount = BigDecimal.ZERO;

    @NotNull
    private LocalDateTime createTime = LocalDateTime.now();

    @NotNull
    private LocalDate issueDate = LocalDate.now();

    @NotNull
    private LocalDate dueDate = LocalDate.now().plusMonths(12);

    @NotNull
    private StatusPayment statusPayment = StatusPayment.UNPAID;

    @NotNull
    private StatusRecord statusRecord = StatusRecord.ACTIVE;
}
