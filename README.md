# Aplikasi Manajemen Piutang #

Fitur :

* Add/Remove Debitur
* Input Piutang
* Input Rencana Pembayaran
* Input Pembayaran
* Satu piutang bisa dibayar dalam beberapa pembayaran
* Satu kali pembayaran bisa melunasi beberapa piutang
* Revisi Rencana Pembayaran
* Pembuatan piutang rutin secara otomatis
* Manajemen Virtual Account

Integrasi :

* BNI Syariah e-Collection
* BSM Virtual Account
* CIMB Syariah Virtual Account

## Cara Menjalankan ##

1. Buat user database PostgreSQL

        createuser -P piutang
        Password : piutang123

2. Buat database untuk user tersebut

        createdb -Opiutang piutangdb

3. Generate skema database

        mvn flyway:migrate \
            -Dflyway.url=jdbc:postgresql://localhost:5432/piutangdb \
            -Dflyway.user=piutang \
            -Dflyway.password=piutang123

4. Jalankan aplikasi

        mvn clean spring-boot:run


